handle('[data-hover-class-scope]', function() {
  
  var self = $(this);
  var target = self.find('[data-hover-class-target]');
  
  self.find('[data-hover-class-trigger]').hover(
    function() {
      target.addClass("active-hover");
    },
    function() {
      target.removeClass("active-hover");
    }
  );
  
});
