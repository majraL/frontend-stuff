$(document).ready(function() {

  function getWindowWidth() {
    return parseFloat($(window).width() + 300);
  }

  function setWindowHeight(windowWidth) {
    $('#heroVideo').css({
      width: windowWidth
    });
  }

  $(window).resize(function() {
    var windowWidth = getWindowWidth();
    setWindowHeight(windowWidth);
  });

});

$(document).one('ready', function() {
  var windowWidthStart = parseFloat($(window).width() + 300);
  $('#heroVideo').css({
    width: windowWidthStart
  });
});
