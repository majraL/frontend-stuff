handle('[data-custom-gallery-scope]', function(){

  var self = $(this);
  var trigger = self.find('[data-custom-gallery-trigger]');
  var galleryItem = self.find('[data-custom-gallery-list] > li:first-of-type [data-galleryItem]');
  
  trigger.on('click', function(){
    galleryItem.click();
  });

});
