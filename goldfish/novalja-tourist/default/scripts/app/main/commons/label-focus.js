handle('.form-group:not(.error.has-error)', function() {
  var self = $(this);
  var input = self.find('.form-control');
  var label = self.find('label');
  
  input.on('click', function() { label.addClass('focus'); });
  input.on('focusout', function() { label.removeClass('focus'); });
});
