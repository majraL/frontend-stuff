handle('[data-sticky-jquery]', function(){

  var self = $(this);
  var stickyOffsetItem = $('[data-sticky-jquery-offset-item]');
  var sumOfHeights = 0;
  
  var stickyInitialize = function() {
    if ($(window).width() > 1024) {
      
      sumOfHeights = 0;
      
      // sum of all element heights under sticky (when sticky must be stopped)
      stickyOffsetItem.each(function() {
        sumOfHeights += $(this).outerHeight();
      });
      
      self.sticky({
        bottomSpacing: (sumOfHeights + self.data('sticky-jquery-offset')) || 0,
        zIndex: self.data('sticky-jquery-z') || 'inherit'
      });
      
    } else {
      self.unstick();
    }
  };
  
  // if sticky exists on page
  if (self.length > 0) {
    $(window).on('resize', function() {
      stickyInitialize(self);
    });

    stickyInitialize(self);
  }
  

});
