handle('[data-action-scope]', function() {
  
  var self = $(this);
  
  self.find('[data-action-trigger]').click(
    function() {
      $(this).toggleClass('active-action');
    }
  );
  
});
