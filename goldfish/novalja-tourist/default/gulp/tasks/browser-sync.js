var browserSync = require('browser-sync').create();
var portscanner = require('portscanner');


module.exports = function(config, gulp, plugins) {
  return function() {
    console.log('---------- Starting BrowserSync server...');


    // Find an available port
    portscanner.findAPortNotInUse(8000, 9000, '127.0.0.1', function(error, port) {
      browserSync.init({
        // port: port,
        // server: "html-app",
        server: config.server,

        // Run in proxy mode with static files also served
        // from current directory + css
        serveStatic: ['.', 'css', 'media'],

        ui: false,
        ghostMode: {
          clicks: true,
          forms: true,
          scroll: true
        },
        reloadDebounce: 200
      });
    });

    // Watch for file changes and trigger browserSync refresh
    gulp.watch(config.watchPath).on('change', browserSync.reload);
  };
};
