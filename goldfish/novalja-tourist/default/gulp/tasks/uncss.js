var uncss = require('gulp-uncss');

module.exports = function(config, gulp, plugins) {
  return function() {
    
    console.log('---------- Removing unused ' + config.instance + ' CSS files...');

    var stream = gulp.src(config.src)
      .pipe(plugins.uncss({
        html: [config.uncssHtmlPath],
        ignore: config.uncssIgnoreSelectors,
        timeout: config.uncssTimeout
      }))
      .pipe(gulp.dest(config.dest));

    return stream;
  };
};
