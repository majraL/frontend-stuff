module.exports = function(path, gulp, plugins) {

  /*-------------------------------------------------------------------------------------------------
    CONFIG
  -------------------------------------------------------------------------------------------------*/
  var config = {
    instanceName: 'App',
    alias: 'app',

    scriptSrcCore: [

      // Node modules
      path.hexis + 'script-core/2.5/*.js'
    ],

    scriptSrcJquery: [

      // Node modules
      path.npm + 'jquery/dist/jquery.js'
    ],

    // Scripts from Goldfish
    scriptSrcGoldfish: [

      // Scripts from goldfish library
      path.root + '../../../../library/webui/handle.js',
      path.root + '../../../../library/webui/shared.js'
    ],

    // Scripts for IMPLEMENTED and STATIC files
    scriptSrcCommons: [
      path.npm + 'jquery-ui/ui/core.js',
      path.npm + 'jquery-ui/ui/datepicker.js',
      path.npm + 'tether/dist/js/tether.js',
      path.npm + 'hexis-commons/components/iconic-script/iconic.js',
      path.npm + 'bootstrap/dist/js/bootstrap.js',
      path.npm + 'lightgallery/dist/js/lightgallery.js',
      path.npm + 'lg-autoplay/dist/lg-autoplay.js',
      path.npm + 'lg-fullscreen/dist/lg-fullscreen.js',
      path.npm + 'lg-hash/dist/lg-hash.js',
      path.npm + 'lg-pager/dist/lg-pager.js',
      path.npm + 'lg-share/dist/lg-share.js',
      path.npm + 'lg-thumbnail/dist/lg-thumbnail.js',
      path.npm + 'lg-video/dist/lg-video.js',
      path.npm + 'lg-zoom/dist/lg-zoom.js',
      path.npm + 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js',

      // Project components
      path.scripts + 'app/main/commons/**/*.js'
    ],

    // Scripts only for IMPLEMENTED files
    scriptSrcImplementation: [

      // Project components
      path.scripts + 'app/main/implementation/**/*.js'
    ],

    // Scripts only for STATIC files
    scriptSrcStatic: [

      // Project components
      path.scripts + 'app/main/static/**/*.js'
    ],

    cssSrc: [

      // Node Modules
      path.npm + 'normalize.css/normalize.css',
      path.npm + 'lightgallery/dist/css/lightgallery.css',
      path.npm + 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css',

      // Main Css files
      path.css + 'app/app.css'
    ]
  };



  /*-------------------------------------------------------------------------------------------------
    COMPILE HTML
  -------------------------------------------------------------------------------------------------*/
  gulp.task('compile-html-'+config.alias, require(path.tasks + 'compile-html')({
    instance: config.instanceName,
    htmlPath: path.html,
    src: path.html + config.alias + '/pages/*.html',
    dest: path.htmlApp
  }, gulp, plugins));
  // Example of executing this task 'gulp compile-html-app'



  /*-------------------------------------------------------------------------------------------------
    COMPILE CSS
  -------------------------------------------------------------------------------------------------*/
  gulp.task('compile-css-'+config.alias, require(path.tasks + 'compile-css')({
    instance: config.instanceName,
    includePaths: [
      path.scss + config.alias + '/',
      path.npm
    ],
    src: path.scss + config.alias + '/**/*.scss',
    dest: path.css + config.alias + '/'
  }, gulp, plugins));
  // Example of executing this task 'gulp compile-css-app'



  /*-------------------------------------------------------------------------------------------------
    CONCATENATE CSS
  -------------------------------------------------------------------------------------------------*/
  gulp.task('concatenate-css-'+config.alias, ['compile-css-'+config.alias], require(path.tasks + 'concatenate-css')({
    instance: config.instanceName,
    src: config.cssSrc,
    concatFilename: 'main.css',
    dest: path.css
  }, gulp, plugins));
  // Example of executing this task 'gulp concatenate-css-app'



  /*-------------------------------------------------------------------------------------------------
    REMOVING UNUSED CSS
  -------------------------------------------------------------------------------------------------*/

  // Selectors which WILL BE INCLUDED into compiled CSS file no matter if they are used or not
  // For finding specific names use regex '/.className/' without quotation marks
  var ignoreSelectors = [
    /.iconic/, /.preformat/, /html.ie/, /.active/, /.open/, /.ui/, /.focus/, /.error/, /.hidden/,
    /.load/, /.selected/, /.hasDatepicker/, /.lg/, /.swiper/, /.form/, /.show/, /.hide/
  ];

  gulp.task('uncss-'+config.alias, require(path.tasks + 'uncss')({
    instance: config.instanceName,
    src: path.css + 'main.css',
    uncssHtmlPath: path.htmlApp + '**/*.html',
    uncssIgnoreSelectors: ignoreSelectors,
    uncssTimeout: 1000,
    dest: path.uncssBuild
  }, gulp, plugins));
  // Example of executing this task 'gulp uncss-app'



  /*-------------------------------------------------------------------------------------------------
    COMPILE SCRIPTS FOR STATIC
  -------------------------------------------------------------------------------------------------*/
  gulp.task('compile-scripts-static-'+config.alias, require(path.tasks + 'compile-scripts')({
    instance: config.instanceName,
    src: config.scriptSrcCore.concat(config.scriptSrcJquery, config.scriptSrcGoldfish,
        config.scriptSrcCommons, config.scriptSrcStatic),
    concatFilename: 'mainStatic.js',
    dest: path.scripts + config.alias + '/build-static/'
  }, gulp, plugins));
  // Example of executing this task 'gulp compile-scripts-static-app'



  /*-------------------------------------------------------------------------------------------------
    COMPILE SCRIPTS FOR IMPLEMENTATION
  -------------------------------------------------------------------------------------------------*/
  gulp.task('compile-scripts-implementation-'+config.alias, require(path.tasks + 'compile-scripts')({
    instance: config.instanceName,
    src: config.scriptSrcCore.concat(config.scriptSrcCommons, config.scriptSrcImplementation),
    concatFilename: 'main.js',
    dest: path.scripts + config.alias + '/build-implementation/'
  }, gulp, plugins));
  // Example of executing this task 'gulp compile-scripts-implementation-app'



  /*-------------------------------------------------------------------------------------------------
    FINAL BUILD TASK
  -------------------------------------------------------------------------------------------------*/
  gulp.task('build-'+config.alias, [
    'compile-html-'+config.alias,
    'compile-css-'+config.alias,
    'concatenate-css-'+config.alias,
    'compile-scripts-static-'+config.alias,
    'compile-scripts-implementation-'+config.alias
  ]);
  // Example of executing this task 'gulp build-app'



  /*-------------------------------------------------------------------------------------------------
    HTML, CSS BUILD TASK
  -------------------------------------------------------------------------------------------------*/
  gulp.task('htmlCss-'+config.alias, [
    'compile-html-'+config.alias,
    'compile-css-'+config.alias,
    'concatenate-css-'+config.alias
  ]);
  // Example of executing this task 'gulp htmlCss-app'



  /*-------------------------------------------------------------------------------------------------
    SERVER
  -------------------------------------------------------------------------------------------------*/
  gulp.task('serve-'+config.alias, ['build-'+config.alias], require(path.tasks + 'browser-sync')({
    watchPath: [
      path.htmlApp + '**/*.html',
      path.css + '**/*.css',
      path.scripts + '**/*.js'
    ],
    server: 'html-app'
  }, gulp, plugins));
  // Example of executing this task 'gulp serve-app'



  /*-------------------------------------------------------------------------------------------------
    WATCH
  -------------------------------------------------------------------------------------------------*/
  gulp.task('watch-'+config.alias, function(){
    console.log('---------- Watching for ' + config.instanceName + ' changes...');

    // HTML, SCSS
    gulp.watch([
      path.html + config.alias + '/**/*.html',
      path.scss + config.alias + '/**/*.scss'
    ], ['htmlCss-'+config.alias]);


    // SCRIPTS IMPLEMENTATION
    gulp.watch([
      path.scripts + config.alias + '/main/implementation/*.js',
      path.scripts + config.alias + '/main/commons/*.js',
      '!' + path.scripts + config.alias + '/build-implementation/'
      // path.hexis + '**/*.js'
    ], ['compile-scripts-implementation-'+config.alias]);

    // SCRIPTS STATIC
    gulp.watch([
      path.scripts + config.alias + '/main/static/*.js',
      path.scripts + config.alias + '/main/commons/*.js',
      '!' + path.scripts + config.alias + '/build-static/'
      // path.hexis + '**/*.js'
    ], ['compile-scripts-static-'+config.alias]);

  });
  // Example of executing this task 'gulp watch-app'



  /*-------------------------------------------------------------------------------------------------
    WORK TASK
  -------------------------------------------------------------------------------------------------*/
  gulp.task(config.alias, [
    'serve-'+config.alias,
    'watch-'+config.alias
  ]);
  // Example of executing this task 'gulp app'

};
