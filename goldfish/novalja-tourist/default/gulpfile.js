/*-------------------------------------------------------------------------------------------------
 Global requirements
 -------------------------------------------------------------------------------------------------*/
var gulp = require('gulp'),
  sourcemaps = require('gulp-sourcemaps'),
  concat = require('gulp-concat'),
  plugins = require('gulp-load-plugins')({lazy:false}),
  svgmin = require('gulp-svgmin');




/*-------------------------------------------------------------------------------------------------
 Paths
 -------------------------------------------------------------------------------------------------*/
var path = { root: __dirname + '/' };
path.instances = path.root + 'gulp/instances/';
path.tasks = path.root + 'gulp/tasks/';
path.htmlApp = path.root + 'html-app/';
path.htmlEmail = path.root + 'html-email/';
path.htmlPdf = path.root + 'html-pdf/';
path.html = path.root + 'html/';
path.scss = path.root + 'scss/';
path.css = path.root + 'css/';
path.uncssBuild = path.root + 'css-uncss/';
path.media = path.root + 'media/';
path.scripts = path.root + 'scripts/';
path.npm = path.root + 'node_modules/';
path.hexis = path.root + 'node_modules/hexis-commons/';




/*-------------------------------------------------------------------------------------------------
 Import instances
 -------------------------------------------------------------------------------------------------*/
require(path.instances + 'app.js')(path, gulp, plugins); // APP
require(path.instances + 'pdf.js')(path, gulp, plugins); // PDF
// For Emails we currently use Foundation Email task runner
// require(path.instances + 'email.js')(path, gulp, plugins); // EMAIL




/*-------------------------------------------------------------------------------------------------
 Default task
 -------------------------------------------------------------------------------------------------*/
gulp.task('default', function(){
    console.log('------- !! Please run a single instance such as "gulp app" !!');
});

gulp.task('svg', function(){
  console.log('.............optimizing SVG images...');
  return gulp.src('media/**/*.svg')
    .pipe(svgmin({
      plugins: [
        {removeDesc: true},
        {removeComments: true},
        {removeTitle: true},
        {minifyStyles: true}
      ]
    }))
    .pipe(gulp.dest('media'));
});
