$(document).ready(function() {

  // Reorder of sidebar elements on mobile/tablet
  if ($(window).width() > 1024) {
    $('[data-sidebar-cut-src]').appendTo($('[data-sidebar-cut-dist]'));
  }

});
