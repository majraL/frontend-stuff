$(document).ready(function() {
  var scroll = new SmoothScroll('[data-scroll]', {
    easing: 'easeInCubic',
    speed: 700,
    updateURL: false
  });
});
