$(document).ready(function() {

  if ($(window).width() > 1024) {
    $("[data-sticky]").stick_in_parent({
      parent: '[data-sticky-parent]',
      offset_top: $('.topbar.-seo').innerHeight()
    });
  }

});
