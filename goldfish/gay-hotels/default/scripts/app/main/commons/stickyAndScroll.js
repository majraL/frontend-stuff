$(document).ready(function() {
  $("[data-stickyFilter]").stick_in_parent({
    parent: '[data-stickyFilter_wrapper]',
    offset_top: parseInt($('[data-sticky-header]').innerHeight() + $('.topbar.-seo').innerHeight())
  });

  if ($(window).width() > 1025) {
    $("[data-search-filter-wrapper]").stick_in_parent({
      sticky_class: 'active-filters',
      offset_top: parseInt($('[data-sticky-header]').innerHeight() + $('.topbar.-seo').innerHeight())
    });
  }

  var scroll = new SmoothScroll('[data-scroll]', {
    header: '[data-sticky-header]',
    easing: 'easeInCubic',
    speed: 700,
    updateURL: false
  });
});
