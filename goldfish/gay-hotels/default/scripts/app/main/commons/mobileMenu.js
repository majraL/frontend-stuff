$(document).ready(function() {

  // Menu / Search
  $('[data-trigger]').click(function () {

    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
      $('[data-' + $(this).attr('data-trigger') + ']').removeClass('active');
      $('.o-canvas').removeClass('active ' + $(this).attr('data-trigger'));
      $('[data-sticky-header]').removeAttr('style');
      $('body').removeClass('overflowHidden');
    } else {
      $('body').addClass('overflowHidden');
      $('[data-trigger]').removeClass('active');
      $('[data-search], [data-menu]').removeClass('active'); // :(

      $(this).addClass('active');
      $('[data-canvas=' + $(this).attr('data-trigger') + ']').addClass('active ' + $(this).attr('data-trigger'));
      $('[data-sticky-header]').not('.p-homepage [data-sticky-header]').css('background', '#0A58FD').addClass('active-search show');
      $('[data-' + $(this).attr('data-trigger') + ']').addClass('active');
    }

  });

  // Menu / Search canvas close
  $('[data-canvas]').on('click', function() {
    $(this).removeClass('active');
    $('[data-' + $(this).data('canvas') + ']').removeClass('active');
    $('[data-trigger=' + $(this).data('canvas') + ']').removeClass('active');
    $('body').removeClass('overflowHidden');
  })

  // Sub Menus
  $('[data-menu-trigger]').click(function() {
    $(this).toggleClass('active');
    $(this).parent($('[data-scope]')).toggleClass('active');
    $(this).parent($('[data-scope]')).find($('[data-' + $(this).attr('data-menu-trigger') + ']')).toggleClass('active');
  });

});
