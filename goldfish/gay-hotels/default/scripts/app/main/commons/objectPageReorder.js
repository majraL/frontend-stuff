$(document).ready(function() {

  // Reorder of sidebar elements on mobile/tablet
  if ($(window).width() < 1025) {
    $('[data-cut-target]').appendTo($('[data-cut-dest]'));
  }
});
