$(document).ready(function() {

  var pages = ['p-eventArticle', 'p-locationArticle', 'p-newsArticle', 'p-static'];
  var search = $('[data-search]');
  var searchContainer = $('[data-paste-search]');

  pages.forEach(function(page) {

    if ($('.' + page).length) {

      if ($(window).width() > 1024) {
        search.appendTo(searchContainer).css('display', 'block');
      }

    }

  });

});
