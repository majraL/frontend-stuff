$(document).ready(function() {
  var destinationSliderItems = $('.o-destinationSlider .o-destination_item').length;

  if (destinationSliderItems === 5) {
    $('.o-destinationSlider').addClass('-fiveItems');
  } else if (destinationSliderItems === 4) {
    $('.o-destinationSlider').addClass('-fourItems');
  } else if (destinationSliderItems === 3) {
    $('.o-destinationSlider').addClass('-threeItems');
  } else if (destinationSliderItems === 2) {
    $('.o-destinationSlider').addClass('-twoItems');
    $('.o-destinationSlider').removeAttr('data-swiper-slides-per-column');
  } else if (destinationSliderItems === 1) {
    $('.o-destinationSlider').addClass('-oneItem');
  }

});
