$(document).ready(function () {

  var stickyItem = $('[data-sticky-header]');

  var search = $('[data-search]');
  var searchFilters = $('[data-search-filter-wrapper]');
  var searchHeight = search.outerHeight();
  var searchPos = search.position();
  var searchHeightTop = +searchHeight + +searchPos.top;

  if ($('body').length) {

    $(window).on('scroll resize', function () {

      // Header
      if (stickyItem.offset().top > 30) {
        stickyItem.addClass('active-stick');
      } else {
        stickyItem.removeClass('active-stick');
      }

    });

  }

  if ($('body').not('.p-search').length) {

    $(window).on('scroll resize', function () {

      // Search
      // TODO: fix for effect to be only on homepage
      if (stickyItem.offset().top > searchHeightTop && $(window).width() < 1025) {
        stickyItem.addClass('active-search').delay(3000).addClass('show');
      } else {
        stickyItem.removeClass('show').delay(3000).removeClass('active-search');
      }

    });

  }

});
