$(document).ready(function () {

  $('[data-search-refine-open]').click(function(){
    $('[data-search-refine-form]').addClass('active')
      .css('top', $('[data-sticky-header]').innerHeight() + $('.topbar.-seo').innerHeight());
  });

  $('[data-search-refine-close]').click(function() {
    $('[data-search-refine-form]').removeClass('active');
  });

});
