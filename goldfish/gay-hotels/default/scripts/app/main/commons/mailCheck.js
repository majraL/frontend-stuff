handle('[data-change-booking-email-form]', function () {

  $('[data-confirmBadtrigger]').on('click', function () {
    $('[data-confirmBad]').removeClass('d-none');
    $('[data-confirmGood]').addClass('d-none');
  });

  $('[data-confirmGoodtrigger]').on('click', function () {
    $('[data-confirmBad]').addClass('d-none');
    $('[data-confirmGood]').removeClass('d-none');
  });

});
