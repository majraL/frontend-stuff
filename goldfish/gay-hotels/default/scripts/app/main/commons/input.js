handle('[data-input]', function () {
  var $this = $(this);

  var toggleActive = function () {
    var self = $(this);
    if (!self.val()) {
      self.parent().removeClass('active');
    } else {
      self.parent().addClass('active');
    }
  };

  $this.focusout(toggleActive);
  $this.change(toggleActive);

  $this.focus(function () {
    $this.parent().addClass('active');
  });

  setTimeout(function () {
    toggleActive.apply($this);
  }, 0);
});
