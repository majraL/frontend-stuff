var initializeSwiper = function (element, swiperIndex) {
  swiperIndex = swiperIndex || null;

  var slidesPerView = parseInt(element.attr('data-swiper-slides-per-view')),
    autoSliderPerView = element.attr('data-swiper-slides-per-view-auto'),
    slideSpeed = parseInt(element.attr('data-swiper-slide-speed')),
    slidesPerGroup = parseInt(element.attr('data-swiper-slides-per-group')),
    spaceBetween = parseInt(element.attr('data-swiper-space-between')),
    slidesPerColumn = element.attr('data-swiper-slides-per-column'),
    slidesPerColumnFill = element.attr('data-swiper-slides-per-column-fill'),
    pagination = element.attr('data-swiper-pagination'),
    paginationObject = element.attr('data-swiper-pagination-object'),
    paginationClickable = element.attr('data-swiper-pagination-clickable'),
    breakpoints = element.attr('data-swiper-breakpoints'),
    autoplay = element.attr('data-swiper-autoplay'),
    autoplayObject = element.attr('data-swiper-autoplay-object'),
    autoplayDisableOnInteraction = element.attr('data-swiper-autoplay-disable-on-interaction'),
    centeredSlides = element.attr('data-swiper-centered-slides'),
    loop = element.attr('data-swiper-loop'),
    loopedSlides = parseInt(element.attr('data-swiper-looped-slides')),
    slidesOffsetBefore = element.attr('data-swiper-offset-before'),
    loopAdditionalSlides = element.attr('data-swiper-loop-additional-slides'),
    initialSlide = element.attr('data-swiper-initial-slide'),
    grabCursor = element.attr('data-swiper-grab-cursor'),
    allowSwipe = element.attr('data-swiper-allow-swiper'),
    allowSwipeToPrev = element.attr('data-swiper-swipe-to-prev'),
    allowSwipeToNext = element.attr('data-swiper-swipe-to-next'),
    swiperEffect = element.attr('data-swiper-effect'),
    swiperSimulateTouch = element.attr('data-swiper-simulate-touch'),
    calcButtonsHeight = element.attr('data-swiper-calc-controls-height'),
    resistanceRatio = parseInt(element.attr('data-swiper-resistance-ratio')),
    navigation = element.attr('data-swiper-navigation'),
    touchRatio = parseInt(element.attr('data-swiper-touch-ratio')),
    slideToClickedSlide = element.attr('data-swiper-slide-to-clicked-slide'),
    swiper = new Swiper(swiperIndex !== null ? ('.swiper-container.instance-' + swiperIndex) : element, {
      nextButton: (pagination && !allowSwipe) ? null : (swiperIndex !== null ?
        ('.swiper-button-next.btn-next-' + swiperIndex) : '.swiper-button-next'),
      prevButton: (pagination && !allowSwipe) ? null : (swiperIndex !== null ?
        ('.swiper-button-prev.btn-prev-' + swiperIndex) : '.swiper-button-prev'),
      slidesPerView: slidesPerView || (autoSliderPerView ? 'auto' : 4),
      slidesPerGroup: slidesPerGroup || 1,
      speed: slideSpeed || 400,
      spaceBetween: spaceBetween || spaceBetween === 0 ? spaceBetween : 5,
      slidesPerColumn: slidesPerColumn || 1,
      slidesPerColumnFill: slidesPerColumnFill || "column",
      pagination: paginationObject ? JSON.parse(paginationObject) : (pagination ? '.swiper-pagination' : null),
      paginationClickable: paginationClickable || false,
      centeredSlides: centeredSlides || false,
      loop: loop || false,
      loopedSlides: loopedSlides || null,
      slidesOffsetBefore: slidesOffsetBefore || 0,
      loopAdditionalSlides: loopAdditionalSlides || 0,
      initialSlide: initialSlide || 0,
      grabCursor: grabCursor || false,
      allowSwipeToPrev: allowSwipeToPrev == null,
      allowSwipeToNext: allowSwipeToNext == null,
      effect: swiperEffect || 'slide',
      simulateTouch: swiperSimulateTouch == null,
      resistanceRatio: resistanceRatio || 0.85,
      navigation: navigation ? JSON.parse(navigation) : {
        nextEl: null,
        prevEl: null
      },
      touchRatio: touchRatio || 1,
      slideToClickedSlide: slideToClickedSlide || false,
      breakpoints: breakpoints ? JSON.parse(breakpoints) : {
        1025: {
          slidesPerView: 3,
          spaceBetween: 5
        },
        800: {
          slidesPerView: 2,
          spaceBetween: 5
        },
        600: {
          slidesPerView: 1,
          spaceBetween: 5
        }
      },
      autoplay: autoplayObject ? JSON.parse(autoplayObject) : (autoplay || 0),
      autoplayDisableOnInteraction: autoplayDisableOnInteraction || true,
      onSlideChangeStart: function(swiper) {
        if (calcButtonsHeight !== undefined) {
          var self = $(swiper.container);
          height = self.find('.swiper-slide-active [data-swiper-calc-controls-height-item]').innerHeight();
          buttons = self.find('[class^="swiper-button-"]');
          buttons.css({
            'height': height,
            'visibility': 'visible'
          });
        }
      },
      onInit: function(swiper) {
        if (calcButtonsHeight !== undefined) {
          var self = $(swiper.container);
          height = self.find('.swiper-slide-active [data-swiper-calc-controls-height-item]').innerHeight();
          buttons = self.find('[class^="swiper-button-"]');
          buttons.css({
            'height': height,
            'visibility': 'visible'
          });
        }
      },
    });

  return swiper;
};

handle('[data-swiper]', function () {
  if ($('[data-multiple-swipers]').length === 0) {
    initializeSwiper($(this));
  }
});

handle('[data-swiper-lazy-load]', function () {
  $(this).on('lazyLoad', function() {
    initializeSwiper($(this));
  });
});

handle('[data-multiple-swipers]', function () {

  var swiperInstances = {};

  $('.swiper-container').each(function(index, element){
    var $this = $(this);

    $this.addClass("instance-" + index);
    $this.find(".swiper-button-prev").addClass("btn-prev-" + index);
    $this.find(".swiper-button-next").addClass("btn-next-" + index);

    swiperInstances[index] = initializeSwiper($this, index);
  });

});
