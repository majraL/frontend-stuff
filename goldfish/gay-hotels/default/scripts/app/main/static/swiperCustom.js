$('#unitModal').on('shown.bs.modal', function (e) {

var galleryTop = new Swiper('.gallery-top', {
     navigation: {
       nextEl: '.swiper-button-next',
       prevEl: '.swiper-button-prev',
     },
     loop: true,
     loopedSlides: 12
   });

   var galleryThumbs = new Swiper('.gallery-thumbs', {
     centeredSlides: true,
     slidesPerView: '12',
     touchRatio: 0.2,
     slideToClickedSlide: true,
     loop: true,
     loopedSlides: 12,

     breakpoints: {

       1024: {
         slidesPerView: 7.5,
       }

     }
   });

   galleryTop.controller.control = galleryThumbs;
   galleryThumbs.controller.control = galleryTop;
});
