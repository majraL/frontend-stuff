String.prototype.replaceArray = function (replaceArray) {
  var replaceString = this.valueOf();
  for (var key in replaceArray) {
    regex = new RegExp(key, "g");
    replaceString = replaceString.replace(regex, replaceArray[key]);
  }
  return replaceString;
};

handle('[data-email-autocorrect]', function () {
  var emailElement = $(this);
  var timeoutId = null;

  var autocorrectWrapper = $('[data-email-autocorrect-wrapper="' + emailElement.attr('name') + '"]').first();
  var templateSuggestionElement = autocorrectWrapper.clone();
  templateSuggestionElement.css({display: ''});

  emailElement.on('keyup', function (e) {

    clearTimeout(timeoutId);

    timeoutId = setTimeout(function () {

      emailElement.mailcheck({
        suggested: function (el, suggestion) {

          autocorrectWrapper.hide();

          var suggestionText = templateSuggestionElement.clone();
          suggestionText.find('[data-email-autocorrect-apply]').attr('data-email-autocorrect-apply',
            emailElement.attr('name'));
          suggestionText.html(suggestionText.html().replaceArray({
            '{{address}}': suggestion.address,
            '{{domain}}': suggestion.domain,
            '{{fullEmail}}': suggestion.full
          }));

          suggestionText.on('click', function (e) {
            e.preventDefault();
            el.val(suggestion.full);
            el.trigger('change');
            $(this).hide();
          });

          autocorrectWrapper.replaceWith(suggestionText);
          autocorrectWrapper = suggestionText;
          autocorrectWrapper.show();

          handleElements();

        },
        empty: function (el) {
          autocorrectWrapper.hide();
        }
      });

    }, 300);

  });

});
