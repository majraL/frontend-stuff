// Popup calendar for input fields
$('[data-datepicker]').datepicker({
  showOtherMonths: true,
  dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"]
});

// Inline months for unit availability
$('#availabilityCalendar').datepicker({
  numberOfMonths: 2
});
