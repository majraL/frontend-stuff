handle('[data-location-dropdown]', function () {

  var wrapper = $(this);
  var label = wrapper.find('[data-location-dropdown-target]');
  var valueElement = wrapper.find('[data-location-dropdown-value-element]');
  var valueElementDisplay = wrapper.find('[data-location-dropdown-value-element-display]');

  valueElementDisplay.on('focus', function (e) {
    e.preventDefault();
    wrapper.addClass('selected');
    $(this).trigger('classIn');
  });

  valueElementDisplay.on('focusout', function (e) {
    var a = $(this);
    setTimeout(function () {
      wrapper.removeClass('selected');
      a.trigger('classOut');
    }, 100);
  });

  wrapper.on('click', '[data-location-dropdown-source]', function (e) {
    e.preventDefault();
    var t = $(this).find('label');

    valueElementDisplay.val(t.text());
    valueElement.val(t.attr('data-value'));

    wrapper.removeClass('selected').addClass('active');

  });

});

$('[data-location-dropdown-mobile]').bind('classIn', function () {
  $('.o-search_filterSubmit').css({
    display: 'none'
  });
});

$('[data-location-dropdown-mobile]').bind('classOut', function () {
  $('.o-search_filterSubmit').css({
    display: ''
  });
});
