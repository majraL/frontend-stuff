var del = require('del');

module.exports = function(config, gulp, plugins) {
  return function() {
    console.log('---------- Deleting ' + config.instance + ' build files...');
    return del(config.delPath);
  };
};
