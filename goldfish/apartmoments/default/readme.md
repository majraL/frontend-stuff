## Installation

* run `nvm install` to change local Node version
* run `npm install` to install all dependencies

## Update package versions
* run `ncu` to execute npm-check-updates package which will display are there any new
  version for packages
* In displayed list check if there are new versions and update them in package.json file
* update every package except 'jquery', in list below are versions
  which must remain
  - jquery: 2.2.2 (stable)
* after updating new versions, remove 'node_modules' folder and execute `npm i`
* update new version in package.json file located in 'goldfish/theme/slice-setup/'

## Running application
* run `gulp app`

* If you receieve an error, it might be cause of node version change, simply run `nvm use`*
* If you receieve an error, it might be cause of new version of some package, so investigate
  which package causes it and revert to last version


## Dependency assets
* directories like `fonts` and/or `img` are copied from `node_modules`, in this case they refer to LightGallery plugin, 
  so all media is loaded properly (images, icons etc)
  
## Possible Todo in future
* create a Gulp task that will copy Dependency assets automatically for all plugins used
* investigate should this assets be junked inside one specific directory with another task that should run  through 
  files and change it's path so assets are not located in multiple directories inside root directory

## Slice setup v1.2
- Long Term Support (LTS) node version 8.9.1 (npm 5.5.1) added
- Bootstrap 4.0.0
