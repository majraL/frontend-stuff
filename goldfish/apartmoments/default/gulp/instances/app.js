module.exports = function(path, gulp, plugins) {

  /*-------------------------------------------------------------------------------------------------
    CONFIG
  -------------------------------------------------------------------------------------------------*/
  var config = {
    instanceName: 'App',
    alias: 'app',

    scriptSrcCore: [

      // Node modules
      path.hexis + 'script-core/2.5/*.js'
    ],

    scriptSrcJquery: [

      // Node modules
      path.npm + 'jquery/dist/jquery.js'
    ],

    // Scripts from Goldfish
    scriptSrcGoldfish: [

      // Scripts from goldfish library
      path.root + '../../../../library/webui/handle.js',
      path.root + '../../../../library/webui/shared.js',
      path.root + '../../../../application/component/Swiper.js'
    ],

    // Scripts for IMPLEMENTED and STATIC files
    scriptSrcCommons: [
      path.npm + 'jquery-ui/ui/core.js',
      path.npm + 'jquery-ui/ui/datepicker.js',
      path.npm + 'tether/dist/js/tether.js',
      path.npm + 'hexis-commons/components/iconic-everything-1.9.0/js/iconic.min.js',
      path.npm + 'popper.js/dist/umd/popper.js',
      path.npm + 'bootstrap/js/dist/util.js',
      path.npm + 'bootstrap/js/dist/tooltip.js',
      path.npm + 'bootstrap/js/dist/modal.js',
      path.npm + 'bootstrap/js/dist/tab.js',
      path.npm + 'swiper/dist/js/swiper.js',
      path.npm + 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js',


      // Project components
      path.scripts + 'app/main/commons/**/*.js'
    ],

    // Scripts only for IMPLEMENTED files
    scriptSrcImplementation: [

      // Project components
      path.scripts + 'app/main/implementation/**/*.js'
    ],

    // Scripts only for STATIC files
    scriptSrcStatic: [

      // Project components
      path.scripts + 'app/main/static/**/*.js'
    ],

    cssSrc: [

      // Node Modules
      path.npm + 'normalize.css/normalize.css',

      // Main Css files
      path.css + 'app/app.css',
      path.npm + 'swiper/dist/css/swiper.css',
      path.npm + 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css',
    ],

    iconicFiles: [
      path.hexisIconic + '/smart/cog.svg'
    ],
  };



  /*-------------------------------------------------------------------------------------------------
    CLEAN BUILD FOLDER
  -------------------------------------------------------------------------------------------------*/

  gulp.task('clean-build-'+config.alias, require(path.tasks + 'clean-build')(
    {
      delPath: [
        'css/' + config.alias + '/**/*',
        'css/main.css',
        'html-' + config.alias + '/**/*',
        'scripts/' + config.alias + '/build-*/**/*',
      ],
    }
  ));



  /*-------------------------------------------------------------------------------------------------
    COMPILE HTML
  -------------------------------------------------------------------------------------------------*/
  gulp.task('compile-html-'+config.alias, require(path.tasks + 'compile-html')({
    instance: config.instanceName,
    htmlPath: path.html,
    src: path.html + config.alias + '/pages/*.html',
    dest: path.htmlApp
  }, gulp, plugins));
  // Example of executing this task 'gulp compile-html-app'



  /*-------------------------------------------------------------------------------------------------
    COMPILE CSS
  -------------------------------------------------------------------------------------------------*/
  gulp.task('compile-css-'+config.alias, require(path.tasks + 'compile-css')({
    instance: config.instanceName,
    includePaths: [
      path.scss + config.alias + '/',
      path.npm
    ],
    src: path.scss + config.alias + '/**/*.scss',
    dest: path.css + config.alias + '/'
  }, gulp, plugins));
  // Example of executing this task 'gulp compile-css-app'



  /*-------------------------------------------------------------------------------------------------
    CONCATENATE CSS
  -------------------------------------------------------------------------------------------------*/
  gulp.task('concatenate-css-'+config.alias, ['compile-css-'+config.alias], require(path.tasks + 'concatenate-css')({
    instance: config.instanceName,
    src: config.cssSrc,
    concatFilename: 'main.css',
    dest: path.css
  }, gulp, plugins));
  // Example of executing this task 'gulp concatenate-css-app'



  /*-------------------------------------------------------------------------------------------------
    COMPILE SCRIPTS FOR STATIC
  -------------------------------------------------------------------------------------------------*/
  gulp.task('compile-scripts-static-'+config.alias, require(path.tasks + 'compile-scripts')({
    instance: config.instanceName,
    src: config.scriptSrcCore.concat(config.scriptSrcJquery, config.scriptSrcGoldfish,
        config.scriptSrcCommons, config.scriptSrcStatic),
    concatFilename: 'mainStatic.js',
    dest: path.scripts + config.alias + '/build-static/'
  }, gulp, plugins));
  // Example of executing this task 'gulp compile-scripts-static-app'



  /*-------------------------------------------------------------------------------------------------
    COMPILE SCRIPTS FOR IMPLEMENTATION
  -------------------------------------------------------------------------------------------------*/
  gulp.task('compile-scripts-implementation-'+config.alias, require(path.tasks + 'compile-scripts')({
    instance: config.instanceName,
    src: config.scriptSrcCore.concat(config.scriptSrcCommons, config.scriptSrcImplementation),
    concatFilename: 'main.js',
    dest: path.scripts + config.alias + '/build-implementation/'
  }, gulp, plugins));
  // Example of executing this task 'gulp compile-scripts-implementation-app'



  /*-------------------------------------------------------------------------------------------------
    HTML, CSS BUILD TASK
  -------------------------------------------------------------------------------------------------*/
  gulp.task('htmlCss-'+config.alias, [
    'compile-html-'+config.alias,
    'compile-css-'+config.alias,
    'concatenate-css-'+config.alias
  ]);
  // Example of executing this task 'gulp htmlCss-app'



  /*-------------------------------------------------------------------------------------------------
    FINAL BUILD TASK
  -------------------------------------------------------------------------------------------------*/
  gulp.task('build-'+config.alias, [
    'compile-html-'+config.alias,
    'compile-css-'+config.alias,
    'concatenate-css-'+config.alias,
    'compile-scripts-static-'+config.alias,
    'compile-scripts-implementation-'+config.alias
  ]);
  // Example of executing this task 'gulp build-app'



  /*-------------------------------------------------------------------------------------------------
    COPY ICONIC FILES
  -------------------------------------------------------------------------------------------------*/
  gulp.task('copy-iconic-'+config.alias, require(path.tasks + 'copy-iconic-icons')({
    instance: config.instanceName,
    src: config.iconicFiles,
    dest: path.media + 'iconic/'
  }, gulp, plugins));
  // Example of executing this task 'gulp copy-iconic-app'



  /*-------------------------------------------------------------------------------------------------
    SERVER
  -------------------------------------------------------------------------------------------------*/
  gulp.task('serve-'+config.alias, ['build-'+config.alias], require(path.tasks + 'browser-sync')({
    watchPath: [
      path.htmlApp + '**/*.html',
      path.css + '**/*.css',
      path.scripts + '**/*.js'
    ],
    server: 'html-app'
  }, gulp, plugins));
  // Example of executing this task 'gulp serve-app'



  /*-------------------------------------------------------------------------------------------------
    WATCH
  -------------------------------------------------------------------------------------------------*/
  gulp.task('watch-'+config.alias, function(){
    console.log('---------- Watching for ' + config.instanceName + ' changes...');

    // HTML, SCSS
    gulp.watch([
      path.html + config.alias + '/**/*.html',
      path.scss + config.alias + '/**/*.scss'
    ], ['htmlCss-'+config.alias]);

    // SCRIPTS IMPLEMENTATION
    gulp.watch([
      path.scripts + config.alias + '/main/implementation/*.js',
      path.scripts + config.alias + '/main/commons/*.js'
    ], ['compile-scripts-implementation-'+config.alias]);

    // SCRIPTS STATIC
    gulp.watch([
      path.scripts + config.alias + '/main/static/*.js',
      path.scripts + config.alias + '/main/commons/*.js'
    ], ['compile-scripts-static-'+config.alias]);

  });
  // Example of executing this task 'gulp watch-app'



  /*-------------------------------------------------------------------------------------------------
    WORK TASK
  -------------------------------------------------------------------------------------------------*/
  gulp.task(config.alias, [
    'clean-build-'+config.alias,
    'serve-'+config.alias,
    'watch-'+config.alias,
    'copy-iconic-'+config.alias
  ]);
  // Example of executing this task 'gulp app'

};
