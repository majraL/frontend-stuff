$(document).ready(function() {
  
  // Active removeR
  function removeActives(el) {

    if (el.hasClass('-active')) {
      el.removeClass('-active');
    }

  }

  function showNavItem(navItem) {
    $('[data-' + navItem + ']').toggleClass('-active');
  }

  function checkBody() {
    if (!$('[data-mobile=search], [data-mobile=menu]').hasClass('-active')) {
      $('body').removeClass('overflowHidden');
    }
  }

  // Menu - Search
  $('[data-mobile]').click(function() {
    if ($(this).data('mobile') === 'search') {
      removeActives($('[data-mobile=menu]'));
      removeActives($('[data-menu]'));
    }
    if ($(this).data('mobile') === 'menu') {
      removeActives($('[data-mobile=search]'));
      removeActives($('[data-search]'));
    }
    $(this).toggleClass('-active');
    showNavItem($(this).data('mobile'));
    $('body').addClass('overflowHidden');
    checkBody();
  });

  // Sub Menu
  $('[data-subTrigger]').click(function() {
    $(this).toggleClass('-activeTrigger');
    $(this).siblings('[data-subMenu]').toggleClass('-active');
  });

  // Language
  $('[data-langCurrent]').click(function() {
    removeActives($('[data-currList]'));
    $('[data-langList]').toggleClass('-active');
  });

  // Currency
  $('[data-currCurrent]').click(function() {
    removeActives($('[data-langList]'));
    $('[data-currList]').toggleClass('-active');
  });

});
