// Popup calendar for input fields
$('[data-datepicker]').datepicker({
  changeMonth: true,
  changeYear: true
});

// Inline months for unit availability
$('#availabilityCalendar').datepicker({
  numberOfMonths: 2
});
