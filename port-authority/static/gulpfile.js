var gulp = require ('gulp');
  browserSync = require ('browser-sync').create(),
  sass = require('gulp-sass'),
  cleanCSS = require ('gulp-clean-css'),
  concat = require('gulp-concat'),
  nunjucks = require('gulp-nunjucks-render'),
  rename = require ('gulp-rename'),
  uglify = require ('gulp-uglify'),
  notify = require ('gulp-notify'),
  svgmin = require('gulp-svgmin'),
  zip = require('gulp-zip'),
  replace = require('gulp-replace'),
  del = require('del'),
  sprite = require('gulp-svg-sprite'),
  tinypng = require ('gulp-tinypng');

//---------------------------------------------------Node dir-----------------------------------------------------------
var path = {
  npm: 'node_modules/'
};
//----------------------------------------------------------------------------------------------------------------------


//--------------------------------------------------BrowserSync---------------------------------------------------------
gulp.task('browser-sync', function () {
  browserSync.init({
    server: {
      baseDir: "build"
    }
  });
});
//----------------------------------------------------------------------------------------------------------------------


//-------------------------------------------CSS minify for build-------------------------------------------------------
gulp.task('sass', function () {
  gulp.src('src/scss/**/*.scss')
    .pipe(sass({
      includePaths: [path.npm]
    }).on('error', function (err) { return notify().write(err); }))
    .pipe(gulp.dest('src/css'))
});
//----------------------------------------------------------------------------------------------------------------------

//---------------------------------------------SVG sprite task---------------------------------------------------------
gulp.task('sprite', function () {
  gulp.src('src/media/symbols/*.svg')
    .pipe(sprite({
      mode: {
        css: {
          render: {
            scss: true,
            scss: {
              dest: '../scss/app/02_tools/_sprite',
            },
          },
          bust: false,
          sprite: '../media/sprite/sprite',
        }
      }
    }).on('error', function (err) { return notify().write(err); }))
    .pipe(gulp.dest('src'))
});
//----------------------------------------------------------------------------------------------------------------------

gulp.task('min', function () {
  gulp.src([
    path.npm + 'normalize.css/normalize.css',
    path.npm + 'lightgallery/dist/css/lightgallery.css',
    'src/css/app/app.css'
  ])
  .pipe(concat('main.css'))
  .pipe(cleanCSS().on('error', function (err) { return notify().write(err); }))
  .pipe(rename({
    suffix: '.min'
  }))
  .pipe(gulp.dest('build/css'))
});


//----------------------------------------HTML copy, find and replace paths---------------------------------------------
gulp.task('html', function () {
  gulp.src('src/html/app/pages/*.html')
    .pipe(nunjucks({
    }).on('error', function (err) { return notify().write(err); }))
    .pipe(gulp.dest('build'))
});
//----------------------------------------------------------------------------------------------------------------------


//-----------------------------------------------Build javascript task--------------------------------------------------
gulp.task('js', function () {
  gulp.src([
    // path.npm + 'hexis-commons/script-core/2.5/*.js',
    path.npm + 'jquery/dist/jquery.min.js',
    path.npm + 'mailcheck/src/mailcheck.min.js',
    // path.npm + 'hexis-commons/components/handle.js/handle.js',
    // path.npm + 'hexis-commons/components/shared.js/shared.js',
    path.npm + 'intl-tel-input/build/js/intlTelInput.js',
    // Light gallery default
    path.npm + 'lightgallery/dist/js/lightgallery.js',

    // Light gallery plugins (uncomment when needed)
    path.npm + 'lg-autoplay/dist/lg-autoplay.js',
    path.npm + 'lg-fullscreen/dist/lg-fullscreen.js',
    path.npm + 'lg-hash/dist/lg-hash.js',
    path.npm + 'lg-pager/dist/lg-pager.js',
    path.npm + 'lg-share/dist/lg-share.js',
    path.npm + 'lg-thumbnail/dist/lg-thumbnail.js',
    path.npm + 'lg-video/dist/lg-video.js',
    path.npm + 'lg-zoom/dist/lg-zoom.js',
    'src/scripts/**/*.js'
  ])
    .pipe(concat('main.js'))
    .pipe(uglify().on('error', function(err) {return notify().write(err);}))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('build/js'))
});
//----------------------------------------------------------------------------------------------------------------------


//------------------------------------------------------TinyPNG task----------------------------------------------------
gulp.task('png', function() {
  gulp.src(['src/media/**/*.png', 'src/media/**/*.jpg'])
    .pipe(tinypng('qvtT2BPQtOvhT-97B47ELxqa-Lcq5FUO'))
    .pipe(gulp.dest('build/media'));
});
//----------------------------------------------------------------------------------------------------------------------


//----------------------------------------------------SVG minify task---------------------------------------------------
gulp.task('svg', function () {
  gulp.src(['src/media/**/*.svg', '!src/media/symbols/*.svg'])
    .pipe(svgmin())
    .pipe(gulp.dest('build/media'));
});
//----------------------------------------------------------------------------------------------------------------------


//----------------------------------------------Optimize/minify PNG/SVG task--------------------------------------------
gulp.task('media', ['png', 'sprite', 'svg']);
//----------------------------------------------------------------------------------------------------------------------


//--------------------------------------Compress all build files to build.zip-------------------------------------------
gulp.task('zip', function () {
  gulp.src('build/**/*.*')
    .pipe(zip('build.zip'))
    .pipe(gulp.dest('./'));
});
//----------------------------------------------------------------------------------------------------------------------


gulp.task('watch', function () {
  gulp.watch('src/scss/**/*.scss', ['sass', 'min']).on('change', browserSync.reload);
  gulp.watch('src/html/**/*.html', ['html']).on('change', browserSync.reload);
  gulp.watch('src/scripts/**/*.js', ['js']).on('change', browserSync.reload);
  gulp.watch('src/media/symbols/*.svg', ['sprite', 'svg', 'sass', 'min', ]).on('change', browserSync.reload);
});


//-------------------------------------------------------Clean task-----------------------------------------------------
gulp.task('clean', function() {
  del(['build.zip']);
});
//----------------------------------------------------------------------------------------------------------------------

//-------------------------------------------CSS minify for build-------------------------------------------------------
gulp.task('cleanCSS', function () {
  del(['build/css']);
});
//----------------------------------------------------------------------------------------------------------------------

//-----------------------------------Copy lightgallery core files-------------------------------------------------------
gulp.task('lg-fonts', function () {
  gulp.src(path.npm + 'lightgallery/dist/fonts/*.*')
  .pipe(gulp.dest('build/fonts'));
});

gulp.task('lg-media', function () {
  gulp.src(path.npm + 'lightgallery/dist/img/*.*')
  .pipe(gulp.dest('build/media/app'));
});

gulp.task('lg', ['lg-fonts', 'lg-media']);
//----------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------Gulp.js build task---------------------------------------------------
gulp.task('app', ['sass', 'min', 'html', 'js', 'sprite', 'svg', 'lg', 'browser-sync', 'watch']);
//----------------------------------------------------------------------------------------------------------------------
