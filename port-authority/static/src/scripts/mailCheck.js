$(document).ready(function() {
  
  var domains = ["yahoo.com", "google.com", "hotmail.com", "gmail.com", "me.com", "aol.com", "mac.com",
    "live.com", "comcast.net", "googlemail.com", "msn.com", "hotmail.co.uk", "yahoo.co.uk",
    "facebook.com", "verizon.net", "sbcglobal.net", "att.net", "gmx.com", "mail.com", "outlook.com", "icloud.com"];
  var topLevelDomains = ["co.jp", "co.uk", "com", "net", "org", "info", "edu", "gov", "mil", "ca"];

  $('[data-email-autocorrect]').on('blur', function() {

    $(this).mailcheck({
      domains: domains,
      topLevelDomains: topLevelDomains,
      suggested: function (element, suggestion) {
        console.log(suggestion);
        $('#mailCheckSuggestion').html('Did you mean ' + suggestion.address + '@<span class="suggestion">' + suggestion.domain + '</span> ?');
      },
      empty: function (element) {
        console.log('empty email address');
      }
    });

  })

});
