$(document).ready(responsiveMenu);
$(window).resize(responsiveMenu);

function responsiveMenu() {
  if($(window).width() > 1024) {

    var mainMenu = $('#dataMenu'),
     menuResponsive = $('[data-responsive-menu-wrapper]'),
     menuResponsiveList = $('[data-responsive-menu]'),
     menuContainer = $('[data-container]'),
     menuResponsiveTrigger = $('.o-mainNav_resMenuWrapper .o-toggle_label'),
     isOuter = menuContainer.offset().left + menuContainer.outerWidth()
      < menuResponsive.offset().left + menuResponsive.outerWidth();

    // Move first menu item back if main menu left offset is more than 500px
    if(mainMenu.offset().left > 500) {
      menuResponsiveList.children().first().appendTo(mainMenu);
    }

    // While responsive menu is outside container move last menu element to responsive menu
    while(isOuter) {
      isOuter = menuContainer.offset().left + menuContainer.outerWidth()
        < menuResponsive.offset().left + menuResponsive.outerWidth();
      mainMenu.children().last().prependTo(menuResponsiveList);
    }

    // Show trigger when responsive list contains items
    if(menuResponsiveList.children().length > 0) {
      menuResponsiveTrigger.addClass('show');
      mainMenu.css('transform', 'translateX(-5rem)');
    } else {
      mainMenu.css('transform', 'translateX(0)');
      menuResponsiveTrigger.removeClass('show');
    }

  }

}
