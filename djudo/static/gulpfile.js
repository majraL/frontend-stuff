var gulp = require ('gulp');
  browserSync = require ('browser-sync').create(),
  sass = require('gulp-sass'),
  cleanCSS = require ('gulp-clean-css'),
  concat = require('gulp-concat'),
  nunjucks = require('gulp-nunjucks-render'),
  rename = require ('gulp-rename'),
  uglify = require ('gulp-uglify'),
  notify = require ('gulp-notify'),
  svgmin = require('gulp-svgmin'),
  zip = require('gulp-zip'),
  replace = require('gulp-replace'),
  del = require('del'),
  tinypng = require ('gulp-tinypng');

//---------------------------------------------------Node dir-----------------------------------------------------------
var dir = {
  npm: 'node_modules'
};
//----------------------------------------------------------------------------------------------------------------------


//--------------------------------------------------BrowserSync---------------------------------------------------------
gulp.task('browser-sync', function () {
  browserSync.init({
    server: {
      baseDir: "build"
    }
  });
});
//----------------------------------------------------------------------------------------------------------------------


//-------------------------------------------CSS minify for build-------------------------------------------------------
gulp.task('sass', function () {
  gulp.src('src/scss/**/*.scss')
    .pipe(sass({
      includePaths: [dir.npm]
    }).on('error', function (err) { return notify().write(err); }))
    .pipe(gulp.dest('build/css'))
});
//----------------------------------------------------------------------------------------------------------------------

gulp.task('min', function () {
  gulp.src([
    'node_modules/normalize.css/normalize.css',
    'build/css/app/app.css'
  ])
  .pipe(concat('main.css'))
  .pipe(cleanCSS().on('error', function (err) { return notify().write(err); }))
  .pipe(rename({
    suffix: '.min'
  }))
  .pipe(gulp.dest('build/css'))
});


//----------------------------------------HTML copy, find and replace paths---------------------------------------------
gulp.task('html', function () {
  gulp.src('src/html/app/pages/*.html')
    .pipe(nunjucks({
    }).on('error', function (err) { return notify().write(err); }))
    .pipe(gulp.dest('build'))
});
//----------------------------------------------------------------------------------------------------------------------


//-----------------------------------------------Build javascript task--------------------------------------------------
gulp.task('js', function () {
  gulp.src([
    'node_modules/hexis-commons/script-core/2.5/*.js',
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/hexis-commons/components/handle.js/handle.js',
    // 'node_modules/hexis-commons/components/shared.js/shared.js',
    'src/scripts/**/*.js'
  ])
    .pipe(concat('main.js'))
    .pipe(uglify().on('error', function(err) {return notify().write(err);}))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('build/js'))
});
//----------------------------------------------------------------------------------------------------------------------


//------------------------------------------------------TinyPNG task----------------------------------------------------
gulp.task('png', function() {
  gulp.src(['src/media/**/*.png', 'src/media/**/*.jpg'])
    .pipe(tinypng('qvtT2BPQtOvhT-97B47ELxqa-Lcq5FUO'))
    .pipe(gulp.dest('build/media'));
});
//----------------------------------------------------------------------------------------------------------------------


//----------------------------------------------------SVG minify task---------------------------------------------------
gulp.task('svg', function () {
  gulp.src('src/media/**/*.svg')
    .pipe(svgmin())
    .pipe(gulp.dest('build/media'));
});
//----------------------------------------------------------------------------------------------------------------------


//----------------------------------------------Optimize/minify PNG/SVG task--------------------------------------------
gulp.task('media', ['png', 'svg']);
//----------------------------------------------------------------------------------------------------------------------


//--------------------------------------Compress all build files to build.zip-------------------------------------------
gulp.task('zip', function () {
  gulp.src('build/**/*.*')
    .pipe(zip('build.zip'))
    .pipe(gulp.dest('./'));
});
//----------------------------------------------------------------------------------------------------------------------


gulp.task('watch', function () {
  gulp.watch('src/scss/**/*.scss', ['sass', 'min']).on('change', browserSync.reload);
  gulp.watch('src/html/**/*.html', ['html']).on('change', browserSync.reload);
  gulp.watch('src/scripts/**/*.js', ['js']).on('change', browserSync.reload);
});


//-------------------------------------------------------Clean task-----------------------------------------------------
gulp.task('clean', function() {
  del(['build.zip']);
});
//----------------------------------------------------------------------------------------------------------------------

//-------------------------------------------CSS minify for build-------------------------------------------------------
gulp.task('cleanCSS', function () {
  del(['build/css']);
});
//----------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------Gulp.js build task---------------------------------------------------
gulp.task('build', ['cleanCSS', 'sass', 'min', 'html', 'js', 'browser-sync', 'watch']);
//----------------------------------------------------------------------------------------------------------------------
