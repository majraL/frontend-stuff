## Installation

To use this template, your computer needs [Node.js](https://nodejs.org/en/) 0.12 or greater. 
The template can be installed with the Foundation CLI, or downloaded and set up manually.

## Using the CLI

Install the Foundation CLI with this command:

```bash
npm install foundation-cli --global
```

Use this command to set up a blank Foundation for Emails project:

```bash
foundation new --framework emails
```

You'll be asked for Project name (use no spaces), after what project dependencies will be installed (it may take a while).

After installation is complete copy/paste following directories from another project already made:
- assets
- layouts
- pages
- partials

Final step is to change:

- Primary & secondary colors in `src/assets/scss/_settings-custom.scss`
- Logo in `src/assets/img/logo.png` (height should be no more then `82px`)

## Running the project

For work run:

`foundation watch`

For final build (inlined and all) run:

`npm run build`
