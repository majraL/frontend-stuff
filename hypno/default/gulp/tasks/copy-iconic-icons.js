module.exports = function(config, gulp, plugins) {
  return function() {
    console.log('---------- Copying ' + config.instance + ' iconic files...');

    var stream = gulp.src(config.src)
      .pipe(gulp.dest(config.dest));

    return stream;
  };
};
