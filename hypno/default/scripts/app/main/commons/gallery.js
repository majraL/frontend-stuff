$(document).ready(function () {

  var self = $('[data-custom-gallery-scope]')
  var trigger = self.find('[data-custom-gallery-trigger]');
  var galleryItem = self.find('[data-galleryItem]');

  self.on('click', function () {
    galleryItem.click();
  });

});
