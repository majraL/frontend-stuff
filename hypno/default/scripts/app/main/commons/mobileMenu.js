$(document).ready(function() {

  var mobileMenu = $("[data-mobileMenu]");
  var headerLogo = $("[data-logo]");

  // TODO
  // put everything in one eventListener

  // Main Menu
  $("[data-mobileMenu-trigger]").click(function() {
    if ($("[data-langList]").hasClass("-active")) {
      $("[data-langList]").removeClass("-active");
    }

    if ($("[data-subMenu]").hasClass("-active")) {
      $("[data-subMenu]").removeClass("-active");
    }

    var mobileTrigger = $(this);
    mobileTrigger.toggleClass("-active");
    mobileMenu.toggleClass("-active");
    headerLogo.toggleClass("-activeMobile");
    $("body").toggleClass("overflowHidden");
  });

  // Sub Menu
    $("[data-subTrigger]").click(function() {
      $(this).toggleClass("-activeTrigger");
      $(this).siblings("[data-subMenu]").toggleClass("-active");
    });

  // Language Menu
  $("[data-langCurrent]").click(function() {
    $(this).next().toggleClass("-active");
  });

});
