$(document).ready(function () {

  // Wordpress fix
  $('.o-mainNav-wp .o-mainNav_item.menu-item-has-children').prepend('<div class="o-mainNav_trigger-sub"></div>');

  $('.o-mainNav-wp .o-mainNav_trigger-sub').click(function() {
    $(this).toggleClass('active').siblings('.sub-menu').toggleClass('open');
  })

  // Menu / Search
  $('[data-trigger]').click(function () {

    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
      $('[data-' + $(this).attr('data-trigger') + ']').removeClass('active');
    } else {
      $(this).addClass('active');
      $('[data-' + $(this).attr('data-trigger') + ']').addClass('active');
    }

  });

  // Sub Menus
  $('[data-menu-trigger]').click(function () {
    $(this).toggleClass('active');
    $(this).parent($('[data-scope]')).toggleClass('active');
    $(this).parent($('[data-scope]')).find($('[data-' + $(this).attr('data-menu-trigger') + ']')).toggleClass('active');
  });

});
