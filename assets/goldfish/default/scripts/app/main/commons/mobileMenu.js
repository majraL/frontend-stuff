$(document).ready(function() {

  var cl = console.log;

  // Menu / Search / Language / Currency triggers
  $('[data-trigger]').click(function () {

    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
      $('[data-' + $(this).attr('data-trigger') + ']').removeClass('active');
    } else {
      $('[data-trigger]').removeClass('active');
      $('[data-search], [data-menu], [data-currency], [data-language]').removeClass('active'); // :(

      $(this).addClass('active');
      $('[data-' + $(this).attr('data-trigger') + ']').addClass('active');
    }

  });

  // Sub Menus
  $('[data-menu-trigger]').click(function() {
    $(this).toggleClass('active');
    $(this).parent($('[data-scope]')).find($('[data-' + $(this).attr('data-menu-trigger') + ']')).toggleClass('active');
  });

});
