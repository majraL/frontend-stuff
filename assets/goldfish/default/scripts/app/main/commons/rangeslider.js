handle('[data-range-slider]', function() {
  var self = $(this);
  var minValue = self.attr('min');
  var maxValue = self.attr('max');
  
  // function for adding classes
  var handleCellClass = function (minValue, maxValue, value, rangeSliderDiv) {
    value = parseInt(value);
    minValue = parseInt(minValue);
    maxValue = parseInt(maxValue);
    
    if (value === maxValue) {
      rangeSliderDiv.find('td:last-of-type').addClass('active-last');
    } else {
      rangeSliderDiv.find('td').eq(value - minValue).addClass('active');
    }
    
  };
  
  // initializing rangeslider
  self.rangeslider({
    polyfill: false
  });
  
  // string with cells which are append into table element
  var cells = "<td></td>".repeat(self.attr('max') - minValue);
  
  // targeting DIV which wraps rangeslider
  var rangeSliderDiv = self.siblings('.rangeslider');

  // appending HTML into DIV
  rangeSliderDiv.append("<table><tbody><tr>" + cells + "</tr></tbody></table>");
  
  // add class to current cell
  handleCellClass(minValue, maxValue, self.val(), rangeSliderDiv);
  
  // on slider (input) change
  self.on('change', function() {
    // remove all classes from cells
    rangeSliderDiv.find('td').removeClass('active active-last');
    
    handleCellClass(minValue, maxValue, $(this).val(), rangeSliderDiv);
  });
  
});
