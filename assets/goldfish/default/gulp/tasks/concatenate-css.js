var cleanCSS = require('gulp-clean-css');

module.exports = function(config, gulp, plugins) {
  return function() {
    console.log('---------- Concatinating ' + config.instance + ' CSS files...');

    var stream = gulp.src(config.src)
      .pipe(plugins.concat(config.concatFilename))
      .pipe(cleanCSS())
      .pipe(gulp.dest(config.dest));

    return stream;
  };
};
