var nunjucksRender = require('gulp-nunjucks-render');
var notify = require('gulp-notify');

module.exports = function(config, gulp, plugins) {
  return function() {
    console.log('---------- Compiling ' + config.instance + ' HTML files...');

    //  We tell Nunjucks where to locate our templates/partials
    nunjucksRender.nunjucks.configure([config.htmlPath], { watch: false });

    var stream = gulp.src(config.src)
      .pipe(nunjucksRender(config.config))
      .on("error", notify.onError({
        message: 'Error: <%= error.message %>',
        sound: 'Purr'
      }))
      .pipe(gulp.dest(config.dest));

    return stream;
  };
};
