module.exports = function(path, gulp, plugins) {

  /*-------------------------------------------------------------------------------------------------
    CONFIG
  -------------------------------------------------------------------------------------------------*/
  var config = {
    instanceName: 'Pdf',
    alias: 'pdf'
  };




  /*-------------------------------------------------------------------------------------------------
    COMPILE HTML
  -------------------------------------------------------------------------------------------------*/
  gulp.task('compile-html-'+config.alias, require(path.tasks + 'compile-html')({
    instance: config.instanceName,
    htmlPath: path.html,
    src: path.html + config.alias + '/pages/*.html',
    dest: path.htmlPdf
  }, gulp, plugins));




  /*-------------------------------------------------------------------------------------------------
    COMPILE CSS
  -------------------------------------------------------------------------------------------------*/
  gulp.task('compile-css-'+config.alias, require(path.tasks + 'compile-css')({
    instance: config.instanceName,
    includePaths: [
      path.scss + config.alias + '/',
      path.npm
    ],
    src: path.scss + config.alias + '/**/*.scss',
    dest: path.css
  }, gulp, plugins));




  /*-------------------------------------------------------------------------------------------------
    INLINE SOURCE
  -------------------------------------------------------------------------------------------------*/
  gulp.task('inline-source-'+config.alias, ['prebuild-'+config.alias], require(path.tasks + 'inline-source')({
    instance: config.instanceName,
    rootPath: path.root,
    src: path.htmlPdf + '*.html',
    dest: path.htmlPdf
  }, gulp, plugins));




  /*-------------------------------------------------------------------------------------------------
    FINAL BUILD TASK
  -------------------------------------------------------------------------------------------------*/
  gulp.task('prebuild-'+config.alias, [
    'compile-html-'+config.alias,
    'compile-css-'+config.alias
  ]);

  gulp.task('build-'+config.alias, [
    'inline-source-'+config.alias
  ]);




  /*-------------------------------------------------------------------------------------------------
    SERVER
  -------------------------------------------------------------------------------------------------*/
  gulp.task('serve-'+config.alias, ['build-'+config.alias], require(path.tasks + 'browser-sync')({
    watchPath: [
      path.htmlPdf + '**/*.html',
      path.css + '**/*.css'
    ],
    server: 'html-pdf'
  }, gulp, plugins));




  /*-------------------------------------------------------------------------------------------------
    WATCH
  -------------------------------------------------------------------------------------------------*/
  gulp.task('watch-'+config.alias, function(){
    console.log('---------- Watching for ' + config.instanceName + ' changes...');

    // HTML, SCSS, Inline Styles
    gulp.watch([
      path.html + config.alias + '/**/*.html',
      path.scss + config.alias + '/**/*.scss',
      path.scss + 'app/**/*.scss'
    ], ['build-'+config.alias]);

  });
  


  /*-------------------------------------------------------------------------------------------------
    WORK TASK
  -------------------------------------------------------------------------------------------------*/
  gulp.task(config.alias, [
    'serve-'+config.alias,
    'watch-'+config.alias
  ]);

};
