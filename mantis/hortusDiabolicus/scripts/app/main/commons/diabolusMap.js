$(document).ready(function() {

  $('#viewport').mapbox({
    defaultLayer: 3,
    doubleClickMove: true // for mobile
  });
  $('#viewport').mapbox('center', { x: 1350, y: 450 });
  var viewport = $('#viewport');

  $('[data-btn-controls]').click(function(e) {
    if ($(e.target).data('zoom') == 'zoom' || $(e.target).data('zoom') == 'back') {
      viewport.mapbox($(e.target).data('zoom'), 2);
    }
    return false;
  });

});
