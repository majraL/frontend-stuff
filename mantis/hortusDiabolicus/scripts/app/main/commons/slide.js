$(document).ready(function() {

  // Debounce
  function debounce(func, wait, immediate) {
    var timeout;
    wait = 10;
    immediate = true;
    return function() {
      var context = this, args = arguments;
      var later = function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      }
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  };

  // Furniture
  var imgFurnitureContainer = document.querySelector("[data-img-furnitureContainer]");
  var imgFurnitures = document.querySelectorAll("[data-img-furnitures]");

  // Categories
  var imgCategoriesContainer = document.querySelector("[data-img-categoriesContainer]");
  var imgCategories = document.querySelectorAll("[data-img-categories]");

  function getSlidePosition(el) {
    var slidePos = (window.scrollY + window.innerHeight) - (el.clientHeight / 4);
    return slidePos;
  }

  function getHalf(pos, el) {
    return pos > el.offsetTop;
  }

  function slide(e) {

    // Furnitures
    imgFurnitures.forEach(function(img) {

      var slideInAt = getSlidePosition(imgFurnitureContainer);
      var isHalfShown = getHalf(slideInAt, imgFurnitureContainer);

      if (isHalfShown) {
        $(img).addClass("active");
      }
    });

    // Categories
    imgCategories.forEach(function(img) {
      
      var slideInAt = getSlidePosition(imgCategoriesContainer);
      var isHalfShown = getHalf(slideInAt, imgCategoriesContainer);

      if (isHalfShown) {
        $(img).addClass('active');
      }
    });
  }

  window.addEventListener("scroll", debounce(slide));

});
