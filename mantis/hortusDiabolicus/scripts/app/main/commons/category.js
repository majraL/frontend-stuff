$(document).ready(function() {

  // Only on tablet and mobile
  if (window.screen.width < 1025) {
    
    var categoryList = $("[data-category='list']");

    // Open category ( Mobile )
    $("[data-categoryMobile]").click(function() {
      var dataset = $(this)[0].dataset.categorymobile;

      if (dataset === "off") {
        $(this)
          .attr("data-categoryMobile", "on")
          .addClass("rotate");
        categoryList.addClass("active");
      } else {
        $(this)
          .attr("data-categoryMobile", "off")
          .removeClass("rotate");
        categoryList.removeClass("active");
      }
    });

    // -------------------------------
    // Add category to list ( Mobile )
    // -------------------------------

    var categoryMobileList = $("[data-category-mobileList]");

    // validate checkbox
    function validateCheckbox(e) {
      if (e.target.checked) {
        return e.target.nodeName === "INPUT" && e.target.attributes.getNamedItem("data-category");
      }
    }

    // set and return object
    function getItem(e) {
      return { name: e.target.labels.item(0).textContent, id: e.target.id };
    }

    // add items to list
    function updateCategoryList(item) {

      if (categoryMobileList[0].children.namedItem("categoryNotice")) {
        $("#categoryNotice").remove();
      }
      
      categoryMobileList.append("<li class='o-category_item-mobile d-inline-flex align-items-center relative' data-category='" + item.id + "'>"
        + "<span class='close d-inline-block text-center' data-category-remove></span>" + item.name + ""
        + "</li>");

    }

    // EventListener
    categoryList.click(function(e) {

      if (validateCheckbox(e)) {
        var itemCheckedLabel = e.target.labels.item(0);
        itemCheckedLabel.classList.add("unactive");
        var itemCheckedObject = getItem(e);
        updateCategoryList(itemCheckedObject);
      }

    });

    // -------------------------
    // Remove category from list
    // -------------------------

    // validate close
    function validateCloseBtn(close) {
      return close.target.nodeName === "SPAN" && close.target.attributes.getNamedItem("data-category-remove");
    }

    // remove from lists
    function removeCategoryItems(item) {
      var id = item.parentNode.dataset.category;
      $(item).parent().remove();
      $("label[for='" + id + "']").removeClass("unactive");
      $("#" + id).trigger("click");
    }

    // EventListener
    categoryMobileList.click(function(e) {

      // Check target
      if (validateCloseBtn(e)) {
        removeCategoryItems(e.target);

        // Check if list is empty
        if (this.children.length === 0) {
          categoryMobileList.append("<li id='categoryNotice' class='d-flex align-items-center ls_8 text-uppercase color-grayLight'>Categories</li>");
        }

      }
    });
  }

});
