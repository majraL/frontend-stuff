$(document).ready(function() {

  var slides = document.querySelectorAll('[data-slide-wrapper]');
  
  // Show first slide
  if (slides.length !== 0) {
    var slideFirst = $(slides[0]);
    var textFirstSlide = slideFirst.find('[data-slide-text]');
    var imgsFirstSlide = slideFirst.find('[data-slide-images]');
    var showFirstSlide = (function() {
      var executed = false;
      return function() {
        if (!executed) {
          executed = true;
          slideFirst.addClass('show active');
          textFirstSlide.addClass('active');
          imgsFirstSlide.each(function(i, img) {
            $(img).addClass('active');
          });
        }
      };
    })();
    showFirstSlide();
  }

  handle('[data-slide-wrapper]', function() {
    var self = $(this);
    var btnNext = self.find('[data-slide-next]');
    var btnPrevious = self.find('[data-slide-prev]');

    function removeActives(text, imgs) {
      text.removeClass('active');
      imgs.each(function(i, img) {
        $(img).removeClass('active');
      });
      // Hide CURRENT slide
      self.removeClass('show active');
    }

    // TODO 
    // check if ALL YTANSITIONS are finished so ALL slides show as expected
    // maybe put check inside removeActives()
    function checkIfDone(slajd) {
      return true;
    }

    function showNextSlide(slide) {
      if (checkIfDone(slide)) {
        slide.next().addClass('show');
        setTimeout(function() {
          slide.next().addClass('active');
        }, 1);
      }
    }

    function showPrevSlide(slide) {
      if (checkIfDone(slide)) {
        slide.prev().addClass('show');
        setTimeout(function() {
          slide.prev().addClass('active');
        }, 1);
      }
    }

    self.find(btnNext).click(function() {

      var slideTextCurrent = self.find('[data-slide-text]');
      var slideImgsCurrent = self.find('[data-slide-images]');
      removeActives(slideTextCurrent, slideImgsCurrent);

      // Show NEXT slide
      showNextSlide(self);

      var slideTextNext = self.next().find('[data-slide-text]');
      var slideImgsNext = self.next().find('[data-slide-images]');

      // on 'transitionend', show TEXT then IMGS
      self.next().on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(e) {
        if (e.originalEvent.propertyName.includes('opacity')) {
          slideTextNext.addClass('active');
          $(slideImgsNext[0]).addClass('active');
          var index = 1;
          var delay = setInterval(function() {
            if (index <= slideImgsNext.length) {
              $(slideImgsNext[index]).addClass('active');
              index += 1;
            } else {
              clearInterval(delay);
            }
          }, 550);
        }
      });

    });

    self.find(btnPrevious).click(function() {

      var slideTextCurrent = self.find('[data-slide-text]');
      var slideImgsCurrent = self.find('[data-slide-images]');
      removeActives(slideTextCurrent, slideImgsCurrent);

      // Show PREV slide
      showPrevSlide(self);

      var slideTextPrev = self.prev().find('[data-slide-text]');
      var slideImgsPrev = self.prev().find('[data-slide-images]');

      // on 'transitionend', show TEXT then IMGS
      self.prev().on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(e) {
        if (e.originalEvent.propertyName.includes('opacity')) {
          slideTextPrev.addClass('active');
          $(slideImgsPrev[0]).addClass('active');
          var index = 1;
          var delay = setInterval(function() {
            if (index <= slideImgsPrev.length) {
              $(slideImgsPrev[index]).addClass('active');
              index += 1;
            } else {
              clearInterval(delay);
            }
          }, 350);
        }
      });

    });
  });

});
