/*-------------------------------------------------------------------------------------------------
  IMPORTS
-------------------------------------------------------------------------------------------------*/
var gulp = require('gulp'),
  argv = require('yargs').argv,
  concat = require('gulp-concat'),
  plugins = require('gulp-load-plugins')({lazy:false});



/*-------------------------------------------------------------------------------------------------
  SETUP
-------------------------------------------------------------------------------------------------*/
var path = { root: __dirname + '/' };
  path.src = path.root + 'src/';
  path.public = 'public';
  path.build = {
    dev: path.root + path.public + '/',
    prod: path.root + path.public + '/'
  };
  path.cache = path.root + 'gulp/cache/';
  path.instances = path.root + 'gulp/instances/';
  path.tasks = path.root + 'gulp/tasks/';
  path.npm = path.root + 'node_modules/';

// Environment
argv.env = argv.env || 'dev';
if (argv.prod) { argv.env = 'prod'; }



/*-------------------------------------------------------------------------------------------------
  AMP
-------------------------------------------------------------------------------------------------*/
path.amp = {
  src: {
    html: path.src + 'html/amp/',
    css: path.src + 'scss/amp/',
    scripts: path.src + 'scripts/amp/',
  },
  dest: {
    html: path.build[argv.env] + 'amp/',
    css: path.build[argv.env] + 'amp/css/',
    scripts: path.build[argv.env] + 'amp/scripts/',
  },
};
require(path.instances + 'amp.js')(path, gulp, plugins, argv);



/*-------------------------------------------------------------------------------------------------
  DEFAULT
-------------------------------------------------------------------------------------------------*/
gulp.task('default', function(){
  console.log('------- !! Please specify an instance such as "gulp-app" !');
});
