var cleanCSS = require('gulp-clean-css');
var rename = require('gulp-rename');

module.exports = function(config, gulp, plugins) {
  return function() {
    console.log('---------- Minifying ' + config.instance + ' CSS files...');
    console.log('           ');

    var stream = gulp.src(config.src)
      // .pipe(plugins.bytediff.start())
      .pipe(cleanCSS({

      }))
      .pipe(rename({ suffix: '.min' }))
      .pipe(plugins.size({
        showFiles: true,
        showTotal: false
      }))
      // .pipe(plugins.bytediff.stop())
      .pipe(gulp.dest(config.dest));

    return stream;
  };
};
