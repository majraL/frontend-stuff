var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');

module.exports = function(config, gulp, plugins) {
  return function() {
    console.log('---------- Compiling ' + config.instance + ' CSS files...');


    var sassSettings = {
      style: 'expanded',
      sync: true,
      errLogToConsole: true,
      includePaths: config.includePaths
    };


    var prefixSettings = [
      'last 3 versions',
      'Explorer >= 10'
    ];


    var stream = gulp.src(config.src)
      .pipe(plumber({
        errorHandler: function(err) {
          notify.onError({
            title:    "SASS Syntax Error",
            message:  "<%= error.message %>",
            sound:    "Purr"
          })(err);
          this.emit('end');
        }
      }))
      .pipe(sass(sassSettings))
      .pipe(autoprefixer(prefixSettings))
      .pipe(gulp.dest(config.dest));

    return stream;
  };
};
