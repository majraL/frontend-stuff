var del = require('del');

module.exports = function(config, gulp, plugins) {
  return function() {
    console.log('---------- Clearing ' + config.log + ' cache');
    return del(config.path);
  };
};
