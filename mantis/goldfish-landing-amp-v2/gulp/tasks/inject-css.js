module.exports = function(config, gulp, plugins) {
  return function() {
    console.log('---------- Injecting CSS into ' + config.instance + ' HTML...');

    var stream = gulp.src(config.src)
      .pipe(plugins.inject(gulp.src(config.css), {
        removeTags: true,
        starttag: '<!-- inject:{{path}} -->',
        transform: function (filePath, file) {
          return file.contents.toString('utf8')
        }
      }))
      .pipe(gulp.dest(config.dest));

    return stream;
  };
};
