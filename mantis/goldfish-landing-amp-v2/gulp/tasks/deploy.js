module.exports = function(config, gulp, plugins) {
  return function() {
    console.log('---------- Deploying ' + config.log + '...');

    var stream = gulp.src(config.src)
      .pipe(gulp.dest(config.dest));

    return stream;
  };
};
