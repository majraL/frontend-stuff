const gulpAmpValidator = require('gulp-amphtml-validator');

module.exports = function(config, gulp, plugins) {
  return function() {

    var stream = gulp.src(config.src)
      .pipe(gulpAmpValidator.validate())
      .pipe(gulpAmpValidator.format())
      .pipe(gulpAmpValidator.failAfterError());

    return stream;
  };
};
