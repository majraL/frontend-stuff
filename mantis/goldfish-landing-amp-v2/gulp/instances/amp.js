module.exports = function(path, gulp, plugins, argv) {


  /*-------------------------------------------------------------------------------------------------
    CONFIG
  -------------------------------------------------------------------------------------------------*/
  var config = {
    instanceName: 'AMP',
    alias: 'amp'
  };



  /*-------------------------------------------------------------------------------------------------
    CLEAR CACHE
  -------------------------------------------------------------------------------------------------*/
  gulp.task('clear-html:'+config.alias, require(path.tasks+'clear-cache')({
    log: config.instanceName+' HTML',
    path: path.cache + config.alias + '/html/**/*'
  }, gulp, plugins));

  gulp.task('clear-css:'+config.alias, require(path.tasks+'clear-cache')({
    log: config.instanceName+' CSS',
    path: path.cache + config.alias + '/css/**/*'
  }, gulp, plugins));

  gulp.task('clear:'+config.alias, [
    config.alias+':'+'clear-html',
    config.alias+':'+'clear-css',
  ]);



  /*-------------------------------------------------------------------------------------------------
    COMPILE CSS
  -------------------------------------------------------------------------------------------------*/
  gulp.task('compile-css:'+config.alias, ['clear-css:'+config.alias], require(path.tasks+'compile-css')({
    instance: config.instanceName,
    includePaths: [
      path[config.alias].src.css,
      path.npm,
      path.npm + '/susy/sass',
    ],
    src: path[config.alias].src.css + '/**/*.scss',
    dest: path.cache + config.alias + '/css/'
  }, gulp, plugins));



  /*-------------------------------------------------------------------------------------------------
    MINIFY CSS
  -------------------------------------------------------------------------------------------------*/
  gulp.task('minify-css:'+config.alias, ['compile-css:'+config.alias], require(path.tasks+'minify-css')({
    instance: config.instanceName,
    src: path.cache + config.alias + '/css/**/*.css',
    dest: path.cache + config.alias + '/css/'
  }, gulp, plugins));



  /*-------------------------------------------------------------------------------------------------
    DEPLOY CSS
  -------------------------------------------------------------------------------------------------*/
  gulp.task('deploy-css:'+config.alias, ['minify-css:'+config.alias], require(path.tasks+'deploy')({
    log: config.instanceName + ' CSS',
    src: path.cache + config.alias + '/css/**/*.css',
    dest: path[config.alias].dest.css
  }, gulp, plugins));



  /*-------------------------------------------------------------------------------------------------
    BUILD CSS (abstraction)
  -------------------------------------------------------------------------------------------------*/
  gulp.task('build-css:'+config.alias, [
    'deploy-css:'+config.alias
  ]);



  /*-------------------------------------------------------------------------------------------------
    COMPILE HTML
  -------------------------------------------------------------------------------------------------*/
  gulp.task('compile-html:'+config.alias, ['clear-html:'+config.alias], require(path.tasks+'compile-html')({
    instance: config.instanceName,
    htmlPath: path[config.alias].src.html,
    src: path[config.alias].src.html + '/pages/*.html',
    dest: path.cache + config.alias + '/html/',
    config: {
      data: {
        mediaPath: '/media/',
      },
    },
  }, gulp, plugins));



  /*-------------------------------------------------------------------------------------------------
    BUILD HTML (abstraction)
  -------------------------------------------------------------------------------------------------*/
  gulp.task('build-html:'+config.alias, [
    'compile-html:'+config.alias
  ]);



  /*-------------------------------------------------------------------------------------------------
    INJECT CSS
  -------------------------------------------------------------------------------------------------*/
  gulp.task('inject-css:'+config.alias, [
    'build-html:'+config.alias,
    'build-css:'+config.alias
  ], require(path.tasks+'inject-css')({
    instance: config.instanceName,
    src: path.cache + config.alias + '/html/**/*.html',
    css: path[config.alias].dest.css + '/**/*.min.css',
    dest: path[config.alias].dest.html,
  }, gulp, plugins));



  /*-------------------------------------------------------------------------------------------------
    VALIDATE
  -------------------------------------------------------------------------------------------------*/
  gulp.task('validate:'+config.alias, ['inject-css:'+config.alias], require(path.tasks+'amp-validate')({
    instance: config.instanceName,
    src: path[config.alias].dest.html + '/**/*.html',
  }, gulp, plugins));



  /*-------------------------------------------------------------------------------------------------
    FINAL BUILD TASK
  -------------------------------------------------------------------------------------------------*/
  gulp.task('build:'+config.alias, [
    'validate:'+config.alias
  ]);



  /*-------------------------------------------------------------------------------------------------
    SERVER
  -------------------------------------------------------------------------------------------------*/
  gulp.task('serve:'+config.alias, ['build:'+config.alias], require(path.tasks+'browser-sync')({
    watchPath: [
      path.public + '/' + config.alias + '/**/*.html',
    ],
    server: {
      baseDir: path.public + '/' + config.alias + '/',
      index: 'index.html',
    }
  }, gulp, plugins));




  /*-------------------------------------------------------------------------------------------------
    WATCH
  -------------------------------------------------------------------------------------------------*/
  gulp.task('watch:'+config.alias, function(){
    console.log('---------- Watching for ' + config.instanceName + ' changes...');

    // HTML
    gulp.watch([
      path[config.alias].src.html + '/**/*.html',
      path[config.alias].src.css + '/**/*.scss',
    ], ['build:'+config.alias]);

  });




  /*-------------------------------------------------------------------------------------------------
    WORK TASK
  -------------------------------------------------------------------------------------------------*/
  gulp.task(config.alias, [
    'serve:'+config.alias,
    'watch:'+config.alias
  ]);

};
